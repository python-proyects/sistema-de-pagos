
# -*- coding: utf-8 -*-
import os, sys
def encabezado():
		print "\n\t----------------------------------------------------------"
		print "\n\t| 	*** SISTEMA DE GESTION DE PAGOS NOVA VISIO ***   |	   Usuario : ",nombre	
		print "\n\t----------------------------------------------------------"	

#comentario para gitlab
def datosTrabajador(datosUsuario):
	bono=0
	sueldoToltal=0
	sueldoBono=0
	datosUsuario={}
	largo=0
	nombreTrabajador=""
	mesCliente=""
	sueldoTrabajador=""
	afpTrabajador=""
	
	while largo!=9 and largo!=10:
		aux=raw_input("\n\t* -Ingrese rut del Cliente : ")
		largo=len(aux)
		if largo==9 or largo==10:
			rut=aux
		else:
			print "\n\t*** El rut esta incorrecto ej: 11111111-1 ***"
#	for llaves in datosUsuario.keys
#		if rut==llaves:
			
	datosUsuario[rut]=[]

	nombreTrabajador=raw_input("\n\t* -Ingrese nombre del trabajador : ")
	largonombre=len(nombreTrabajador)
	while largonombre<3:
		nombreTrabajador=raw_input("\n\t *** Debe ingresar por lo menos 3 caracteres : ***")
		largonombre=len(nombreTrabajador)
	datosUsuario[rut].append(nombreTrabajador)


	mesCliente=raw_input("\n\t* -Ingrese mes de la liquidacion : ")
	largoMes=len(mesCliente)
	while largoMes<4:
		mesCliente=raw_input("\n\t *** Debe ingresar por lo menos 4 caracteres : ***")
		largoMes=len(mesCliente)
	while  mesCliente!="Enero" and mesCliente!="Febrero" and mesCliente!="Marzo" and mesCliente!="Abril" and mesCliente!="Mayo" and mesCliente!="Junio" and mesCliente!="Julio" and mesCliente!="Agosto" and mesCliente!="Septiembre" and mesCliente!="Octubre" and mesCliente!="Noviembre" and mesCliente!="Diciembre":
		mesCliente=raw_input("\n\t*** Favor ingrese un mes valido Ej. Enero  : ***")
	datosUsuario[rut].append(mesCliente)
	
	
	anoCliente=raw_input("\n\t* -Ingrese año de la liquidacion : ")
	largoAno=len(anoCliente)
	while largoAno!=4:
		anoCliente=raw_input("\n\t *** Año ingresado no valido, favor ingresar ano liquidacion : ***")
		largoAno=len(anoCliente)
	datosUsuario[rut].append(anoCliente)
	
	categoriaCliente=raw_input("\n\t* -Ingrese la categoria del Trabajador Ej. Novato , Experto , Supervisor , Adminitrativo : ")
	largoCategoria=len(categoriaCliente)
	while largoCategoria==0:
		categoriaCliente=raw_input("\n\t *** El campo no puede estar en blanco, ingrese categoria  : ***")
		largoCategoria=len(categoriaCliente)
	while  categoriaCliente!="Novato" and categoriaCliente!="Experto" and categoriaCliente!="Supervisor" and categoriaCliente!="Administrativo":
		categoriaCliente=raw_input("\n\t *** La opcion ingresada no es valida, por favor ingresar categoria : ***")
	datosUsuario[rut].append(categoriaCliente)
	
	diasAusenciaString=raw_input("\n\t* -Favor Ingresar dias de ausencia del trabajador : ")
	largodias=len(diasAusenciaString)
	while largodias==0:
		diasAusenciaString=raw_input("\n\t **** El campo no puede estar vacio *** , Ingrese dias de ausencia : ")
		largodias=len(diasAusenciaString)
	if largodias!=0:
		diasAusencia=int(diasAusenciaString)
	while diasAusencia<-1 or diasAusencia>30:
		diasAusencia=input("\n\t **** Los dias de ausencia no pueden ser menor a 0 o mayor 40, favor ingresar cantidad de dias : ***")
	datosUsuario[rut].append(diasAusencia)	
	
	afpTrabajador=raw_input("\n\t* -Favor ingresar AFP del trabajador : ")
	largoAfp=len(afpTrabajador)
	while largoAfp==0:
		afpTrabajador=raw_input("\n\t*** El campo no puede estar en blanco, ingrese AFP  : ***")
		largoAfp=len(afpTrabajador)	
	datosUsuario[rut].append(afpTrabajador)
	
	saludTrabajador=raw_input("\n\t* -Favor ingresar Sistema de Salud del trabajador Ej. A, B o C : ")
	largoSalud=len(saludTrabajador)
	while largoSalud==0:
		saludTrabajador==raw_input("\n\t *** El campo no puede estar en blanco, ingrese Sistema de Salud  : ***")
		largoSalud=len(saludTrabajador)	
	while  saludTrabajador!="A" and saludTrabajador!="B" and saludTrabajador!="C":
		saludTrabajador=raw_input("* -Favor ingresar Sistema de salud valido Ej. A , B o C : ")
	datosUsuario[rut].append(saludTrabajador)
	
	sueldoTrabajadorString=raw_input("\n\t* -Favor Ingresar sueldo bruto del trabajador : ")
	largoSueldo=len(sueldoTrabajadorString)
	while largoSueldo==0:
		sueldoTrabajadorString=raw_input("\n\t **** El campo no puede estar vacio *** , Ingrese sueldo bruto : ")
		largoSueldo=len(sueldoTrabajadorString)
	if largoSueldo!=0:
		sueldoTrabajador=int(sueldoTrabajadorString)

	while sueldoTrabajador<0 and sueldoTrabajador==0:
		sueldoTrabajador=input(" *** El sueldo bruto no puede ser 0 o inferior a 0, favor ingresar sueldo bruto : ***")
	datosUsuario[rut].append(sueldoTrabajador)	


	afp=datosUsuario[rut][7]*0.1
	datosUsuario[rut].append(afp)
	

	
	if saludTrabajador=="A":
		A=(datosUsuario[rut][7]*5.7)/100
		datosUsuario[rut].append(A)
	elif saludTrabajador=="B":
		B=(datosUsuario[rut][7]*6.1)/100
		datosUsuario[rut].append(B)
	elif saludTrabajador=="C":
		C=(datosUsuario[rut][7]*6.5)/100
		datosUsuario[rut].append(C)
		
		
	sueldoTotal=datosUsuario[rut][7]-datosUsuario[rut][8]-datosUsuario[rut][9]
	datosUsuario[rut].append(sueldoTotal)
	
	if diasAusencia==0:
		bono=50000
		sueldoBono=datosUsuario[rut][10]+bono
		datosUsuario[rut].append(sueldoBono)
		datosUsuario[rut].append(bono)
		if categoriaCliente=="Experto":
			sueldoBono=sueldoBono+bono
			datosUsuario[rut][11]=sueldoBono
			bono=bono+50000
			datosUsuario[rut][12]=bono
			
	else:
		datosUsuario[rut].append(sueldoTotal)
		datosUsuario[rut].append(bono)
	
	
	return datosUsuario

def busqueda(datosM):
	
	salir=""
	cont=0
	criterio=0
	salirA="si"
	while criterio!="6":
		os.system("cls")
		encabezado()
		salirA="si"
		#asdasprint datosM
		print "\n\t1.- Busqueda por Mes"
		print "\t2.- Busqueda por Año"
		print "\t3.- Busqueda por Rut"
		print "\t4.- Listar liquidaciones"	
		print "\t5.- Buscar por Mes - Año"
		print "\t6.- Volver al menu principal"
			
		criterio=raw_input("\n\tElegir un criterio de busqueda de Liquidaciones: ")
		
		if criterio=="1":
			while salirA!="no":
				cont=0
				os.system("cls")
				encabezado()
				mes=raw_input("\n\t* -Ingresar mes a consultar : ")
				while mes!="Enero" and mes!="Febrero" and mes!="Marzo" and mes!="Abril" and mes!="Mayo" and mes!="Junio" and mes!="Julio" and mes!="Agosto" and mes!="Septiembre" and mes!="Octubre" and mes!="Noviembre" and mes!="Diciembre":
					mes=raw_input("\n\tFavor ingrese un mes valido Ej. Enero  : ")
				print "\n\tRUT TRABAJADOR		NOMBRE TRABAJADOR	CATEGORIA		AÑO		SUELDO LIQUIDO"
				print "	--------------		-----------------	----------		---		-------------"
				for llaves in datosM.keys():
					if mes==datosM[llaves][1]:
						cont=cont+1
						print "\n\t",llaves,"		",datosM[llaves][0],"		",datosM[llaves][3],"			",datosM[llaves][2],"		$",datosM[llaves][11]
				if cont==0:
					print "\n\t *** No se encontraron resultados para esta busqueda ***"
				salirA=raw_input("\n\tDese realizar otra busqueda si/no : 	")
				
		if criterio=="2":
			while salirA!="no":
				cont=0
				os.system("cls")
				encabezado()
				ano=raw_input("\n\t* -Ingresar año a consultar : ")
				largoAno=len(ano)
				while largoAno!=4:
					ano=raw_input("\n\tEl Año debe tener 4 digitos, ingrese nuevamente el año :  ")
					largoAno=len(ano)
				print "\n\tRUT TRABAJADOR		NOMBRE TRABAJADOR	   MES			AÑO		SUELDO LIQUIDO"
				print "	--------------		-----------------	  -----			---		-------------"
				for llaves in datosM.keys():
					if ano==datosM[llaves][2]:
						cont=cont+1
						print "\n\t",llaves,"		",datosM[llaves][0],"		 ",datosM[llaves][1],"		",datosM[llaves][2],"		$",datosM[llaves][11]
				if cont==0:
					print "\n\t *** No se encontraron resultados para esta busqueda ***"
				salirA=raw_input("\n\tDese realizar otra busqueda si/no : 	")	
		
		if criterio=="3":
			salirD="si"
			while salirD!="no":
				cont=0
				os.system("cls")
				encabezado()
				largoRut=0
				while largoRut!=9 and largoRut!=10:
					aux=raw_input("\n\t* -Ingrese rut del Cliente a buscar : ")
					largoRut=len(aux)
					if largoRut==9 or largoRut==10:
						rut=aux
					else:
						print "\n\t*** El rut esta incorrecto ej: 11111111-1 ***"

				for llaves in datosM.keys():
					if rut==llaves:
						cont=cont+1
						print "\n\t----------------------------------------------------------------------------"
						print "\n\t|		LIQUIDACION DE SUELDO MES DE",datosM[llaves][0],"			|"
						print "\n\t----------------------------------------------------------------------------"
						print u"\n\tMES : ",datosM[llaves][1],"						AÑO :",datosM[llaves][2]
						print "\n\tRut del trabajador : ",rut
						print "\n\tNombre del trabajador : ", datosM[llaves][0],"		CATEGORIA	: ",datosM[llaves][3]
						print "\n\tDias de ausencia : ",datosM[llaves][4],"					AFP		: ",datosM[llaves][5]		 
						print "\n\n\t Detalle de Valores"
						print "\t ------------------"
						print "\n\tSueldo Bruto :			 $",datosM[llaves][7],"		Descuento Afp	: 		 $",datosM[llaves][8]
						print "\tBono 100 % asistencia :	   	 $",datosM[llaves][12],"		Descuento Salud clase,",datosM[llaves][6],":	 $",datosM[llaves][9]
						print "\n\t-------------------------------------------------------------------------------------------------------"
						print "\n\tTOTAL SUELDO LIQUIDO 									 $",datosM[llaves][11]
				if cont==0:
					print "\n\t *** No se encontraron resultados para esta busqueda ***"
					
						
					
				salirD=raw_input("\n\tDesea Buscar otro Rut ? ej. si / no :")	
				
		if criterio=="4":
			cont=0
			os.system("cls")
			encabezado()
			for llaves in datosM.keys():
				cont=cont+1
				print "\n\t----------------------------------------------------------------------------"
				print "\n\t|		LIQUIDACION DE SUELDO MES DE",datosM[llaves][0],"			|"
				print "\n\t----------------------------------------------------------------------------"
				print u"\n\tMES : ",datosM[llaves][1],"						AÑO :",datosM[llaves][2]
				print "\n\tRut del trabajador : ",llaves
				print "\n\tNombre del trabajador : ", datosM[llaves][0],"		CATEGORIA	: ",datosM[llaves][3]
				print "\n\tDias de ausencia : ",datosM[llaves][4],"					AFP		: ",datosM[llaves][5]		 
				print "\n\n\t Detalle de Valores"
				print "\t ------------------"
				print "\n\tSueldo Bruto :			 $",datosM[llaves][7],"		Descuento Afp	: 		 $",datosM[llaves][8]
				print "\tBono 100 % asistencia :	   	 $",datosM[llaves][12],"		Descuento Salud clase,",datosM[llaves][6],":	 $",datosM[llaves][9]
				print "\n\t-------------------------------------------------------------------------------------------------------"
				print "\n\tTOTAL SUELDO LIQUIDO 									 $",datosM[llaves][11]
			if cont==0:
				print "\n\t *** No se encontraron resultados para esta busqueda ***"
			
			print "\n\n\t*** Presione ENTER para volver al menu principal. ***"
			salir=raw_input()	
		
		
		
		
		if criterio=="5":
			while salirA!="no":
				cont=0
				os.system("cls")
				encabezado()
				mes=raw_input("\n\t* -Ingresar mes a consultar : ")
				while mes!="Enero" and mes!="Febrero" and mes!="Marzo" and mes!="Abril" and mes!="Mayo" and mes!="Junio" and mes!="Julio" and mes!="Agosto" and mes!="Septiembre" and mes!="Octubre" and mes!="Noviembre" and mes!="Diciembre":
					mes=raw_input("\n\t *** Ingresa mes correcto Ej. Enero : ")
				ano=raw_input("\n\t* -Ingresar año a consultar : ")
				largoAno=len(ano)
				while largoAno!=4:
					ano=raw_input("\n\tEl Año debe tener 4 digitos, ingrese nuevamente el año :  ")
					largoAno=len(ano)

				print "\n\tRUT TRABAJADOR		NOMBRE TRABAJADOR	   MES			AÑO		SUELDO LIQUIDO"
				print "	--------------		-----------------	  -----			---		-------------"
				for llaves in datosM.keys():
					if mes==datosM[llaves][1] and ano==datosM[llaves][2]:
						cont=cont+1
						print "\n\t",llaves,"		",datosM[llaves][0],"		 ",datosM[llaves][1],"		",datosM[llaves][2],"		$",datosM[llaves][11]
				if cont==0:
					print "\n\t *** No se encontraron resultados para esta busqueda ***"
				salirA=raw_input("\n\tDese realizar otra busqueda si/no : 	")										


print "\n\t----------------------------------------------------------"
print "\n\t| 	*** SISTEMA DE GESTION DE PAGOS NOVA VISIO ***   |	 "
print "\n\t----------------------------------------------------------"	

largonombre=0

nombre=raw_input("\n\t\t\tUsuario : ")
largonombre=len(nombre)

while largonombre==0:
	nombre=raw_input("\n\t*** El campo usuario no puede estar en blanco, ingrese nombre de usuario *** :   ")
	largonombre=len(nombre)
password=raw_input("\n\t\t\tPassword : ")
opcion=""
contrasena="532"
cont=0
datosUsuario=dict()
datosM=dict()
largoRut=0
salirD="si"
#datosM={'13414586-2': ['Gabriel Prieto', 'Julio', '2017', 'Novato', 0, 'Habitat', 'A',560000, 56000.0, 34770.0, 469230, 519230, 50000]}
datosM={'12333333-2': ['Joaquin Prieto', 'Septiembre', '2017', 'Supervisor', 2, 'Cuprum', 'C', 600000, 60000.0, 39000.0, 501000.0, 501000.0, 0], '13414586-2': ['Gabriel Prieto', 'Julio', '2017', 'Novato', 0, 'Habitat', 'A', 560000, 56000.0, 34770.0, 469230, 519230, 50000], '22222222-2': ['Sistemas Exp.', 'Agosto', '2017', 'Experto', 0, 'Provida', 'B', 550000, 55000.0, 33550.0, 461450.0, 611450.0, 100000]}
#datosM={'12333333-2': ['Joaquin Prietos', 'Septiembre', '2017', 'Supervisor', 2,
#				'Cuprum', 'C', 600000, 60000.0, 39000.0, 501000.0, 501000.0, 0],
#				 '13414586-2': ['Gabriel Prietos', 'Julio', '2017', 'Novato', 0, 'Habitat', 'A', 560000, 
#				 56000.0, 34770.0, 469230, 519230, 50000], '11111111-2': ['Vladimir Prieto', 'Julio', '2017', 
#				 'Experto', 0, 'Cuprum', 'C', 600000, 60000.0, 39000.0, 501000.0, 601000.0, 50000],
#				  '25656985-2': ['Patricia Britos', 'Julio', '2017', 'Experto', 3, 'Provida', 'A', 900000, 90000.0, 51300.0, 758700.0, 758700.0, 0],
#				   '22222222-2': ['Sistemas Exps.', 'Agosto', '2017', 'Experto', 0, 'Provida', 'B',
#				    550000, 55000.0, 33550.0, 461450.0, 611450.0, 100000]}
while cont<2:
	if contrasena==password:
		salirD="si"
		os.system("cls")
		print "\n\t----------------------------------------------------------"
		print "\n\t| 	*** SISTEMA DE GESTION DE PAGOS NOVA VISIO ***     |	   Usuario : ",nombre	
		print "\n\t----------------------------------------------------------"	
		
		print "\n\ta.- Ingresar Datos"
		print "\tb.- Calcular Sueldos"
		print "\tc.- Listar Empleados"
		print "\td.- Listar Liquidaciones"
		print "\ts.- Salir del sistema"
		
		opcion=raw_input("\n\tIngresa una de las opciones : ")
		
		if opcion=="a":
			
			os.system("cls")
			encabezado()
			datosM.update(datosTrabajador(datosUsuario))
			salirb="si"

			while salirb!="no":
				salirb=raw_input("\n\t *** Desea Ingresar otro despacho ? si , no  ***:	")
				if salirb=="si":
					os.system("cls")
					encabezado()
					datosM.update(datosTrabajador(datosUsuario))
				else:
					break
		
		if opcion=="b":
			while salirD!="no":
				os.system("cls")
				encabezado()
				largoRut=0
				while largoRut!=9 and largoRut!=10:
					aux=raw_input("\n\tIngrese rut del Cliente a buscar : ")
					largoRut=len(aux)
					if largoRut==9 or largoRut==10:
						rut=aux
					else:
						print "\n\t*** El rut esta incorrecto ej: 11111111-1 ***"
				
				for llaves in datosM.keys():
					if rut==llaves:
						print "\n\t----------------------------------------------------------------------------"
						print "\n\t|		LIQUIDACION DE SUELDO MES DE",datosM[llaves][0],"			|"
						print "\n\t----------------------------------------------------------------------------"
						print u"\n\tMES : ",datosM[llaves][1],"						AÑO :",datosM[llaves][2]
						print "\n\tRut del trabajador : ",rut
						print "\n\tNombre del trabajador : ", datosM[llaves][0],"		CATEGORIA	: ",datosM[llaves][3]
						print "\n\tDias de ausencia : ",datosM[llaves][4],"					AFP		: ",datosM[llaves][5]		 
						print "\n\n\t Detalle de Valores"
						print "\t ------------------"
						print "\n\tSueldo Bruto :			 $",datosM[llaves][7],"		Descuento Afp	: 		 $",datosM[llaves][8]
						print "\tBono 100 % asistencia :	   	 $",datosM[llaves][12],"		Descuento Salud clase,",datosM[llaves][6],":	 $",datosM[llaves][9]
						print "\n\t-------------------------------------------------------------------------------------------------------"
						print "\n\tTOTAL SUELDO LIQUIDO 									 $",datosM[llaves][11]

						
					
				salirD=raw_input("\n\tDesea imprimir otra Liquidacion ? ej. si / no :")	
		
		if opcion=="c":
			os.system("cls")
			encabezado()
			print "\n\tRUT TRABAJADOR			NOMBRE TRABAJADOR		CATEGORIA"
			print "\t--------------			-----------------		----------"
			for llaves in datosM.keys():
				print "\n\t",llaves,"			",datosM[llaves][0],"			",datosM[llaves][3]
			
		
			print "\n\n\t*** Presione ENTER para volver al menu principal. ***"
			salir=raw_input()
			
		if opcion=="d":
			os.system("cls")
			encabezado()
			busqueda(datosM)

			
		if opcion=="s":
			break			
					
		
	else:
		password=raw_input("\n\t\t\tPassword : ")
		cont=cont+1

if opcion=="s":
	os.system("cls")
	encabezado()
	print "\n\t *** Sistema cerrado, hasta pronto ! ***"
	
else:
	print "\n\t	*** Lo sentimos  el sistema se ha bloqueado ***"	
